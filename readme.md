This bot operates with OpenCV. It exclusivly works on Linux, it's been tested on Ubuntu 18.04!

You need to install:

    sudo apt install xdotool libx11-dev wmctrl



To use the bot simply open your browser window and open up your game. Choose a planet
and start the bot, it will now check for free tiles and play them. The bot will stop after
it has checked all 50 tiles. Then you need to manually set a new planet and start up the bot
again.
