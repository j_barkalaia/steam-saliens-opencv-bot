/**
 * optionally compile with:
 * g++ -o saliens_bot main.cpp `pkg-config --libs opencv`
 */
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <X11/Xlib.h>
#include <chrono>
#include <climits>
#include <cstdio>
#include <ctime>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <unistd.h>
#include "img_base.c"
cv::Mat img;
cv::Mat templ(temple.height, temple.width, CV_8UC3, (void*)temple.pixel_data);
cv::Mat result;

int match_method; //We use the SQDIFF method as it works best with detecting the "ok" button

using namespace std::chrono_literals; // We can use seconds now

typedef std::chrono::high_resolution_clock Clock;

int clickAt(int x, int y) // Using xdotool to click at a specific spot determined by x / y
{
    std::ostringstream s;
    std::this_thread::sleep_for(0.3s);
    s << "xdotool search --name 'Steam Community - Mozilla Firefox' mousemove "
         "--window %1 "
      << x << " " << y;
    system(s.str().c_str());
    std::this_thread::sleep_for(0.2s); // Delay so the mouse has enough time to actually move
    return system("xdotool click 1");
}

bool MatchingMethod(int, void*) // Checks if it can find an "okay" button
{
    match_method = 0; //0 stands for SQDIFF
    /// Source image to display
    cv::Mat img_display;
    img.copyTo(img_display);

    /// Create the result matrix
    int result_cols = img.cols - templ.cols + 1;
    int result_rows = img.rows - templ.rows + 1;

    result.create(result_rows, result_cols, CV_32FC1);

    /// Do the Matching and Normalize
    cv::matchTemplate(img,
        templ,
        result,
        match_method); // Move the template image 1 pixel by one
    cv::normalize(result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

    /// Localizing the best match with minMaxLoc
    double minVal;
    double maxVal;
    cv::Point minLoc;
    cv::Point maxLoc;
    cv::Point matchLoc;

    cv::minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat());

    /// For SQDIFF and SQDIFF_NORMED, the best matches are lower values. For all
    /// the other methods, the higher the better

    if (match_method == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED) {
        matchLoc = minLoc;
    } else {
        matchLoc = maxLoc;
    }

    /// Was okay found?

    return (cv::Point(matchLoc.x + templ.cols + 30, matchLoc.y + templ.rows - 30) == cv::Point(1117, 582));
}

int pressOne(int x, int y) // Same thing as the click we just press 1
{
    std::ostringstream s;
    s << "xdotool search --name 'Steam Community - Mozilla Firefox' mousemove "
         "--window %1 "
      << x << " " << y << " key 1";
    return system(s.str().c_str());
}

int enter() // Same thing as the click we just press Enter
{
    std::ostringstream s;
    std::this_thread::sleep_for(0.3s);
    s << "xdotool search --name 'Steam Community - Mozilla Firefox' key KP_Enter";
    return system(s.str().c_str());
}

int takeScreenshot() //using imagemagick's import to take a screenshot
{
    //storing it inside /tmp/ to accelerate the process
    system("import -window 'Steam Community - Mozilla Firefox' /tmp/shot.ppm");
}

int main(int, char**)
{
    std::this_thread::sleep_for(2s);
    // Adjust the window's properties to work with it more easily
    system("wmctrl -r Steam Community - Mozilla Firefox -b remove,fullscreen");
    system(
        "wmctrl -r Steam Community - Mozilla Firefox -b remove,maximized_vert");
    system(
        "wmctrl -r Steam Community - Mozilla Firefox -b remove,maximized_horz");
    // Scale the window
    system("wmctrl -r Steam Community - Mozilla Firefox -e 0,0,0,1400,1000");

    int horpos = 330;
    int zaehler = 1;
    int counter = 1;
    while (counter < 50) {
        std::this_thread::sleep_for(2s);
        takeScreenshot();

        /// Load image and template

        img = cv::imread("/tmp/shot.ppm");


        clickAt(490, 330);
        std::this_thread::sleep_for(2s);
        takeScreenshot();
        img = cv::imread("/tmp/shot.ppm");

        // We go through every tile and check whether it is availible
        while (MatchingMethod(0, 0) == 1) {
            counter++;
            enter();
            clickAt(490 + (70 * zaehler), horpos);
            zaehler++;
            if (zaehler > 11) {
                horpos = horpos + 60;
                zaehler = 0;
            }
            std::this_thread::sleep_for(0.3s);
            system(
                "import -window 'Steam Community - Mozilla Firefox' /tmp/shot.ppm");
            img = cv::imread("/tmp/shot.ppm");
        }

        /// Play the game
        // We measure the time (one round lasts 120 seconds, we use 135
        // because the starting animation's pretty long)
        double time;
        auto t1 = Clock::now();
        auto t2 = Clock::now();

        while (time < 135) {
            takeScreenshot();
            cv::Mat image = cv::imread("/tmp/shot.ppm");
            cv::Point offset(399, 440);
            cv::Size game_area(930, 423);
            image = image(cv::Rect(
                offset,
                game_area)); // Cropping the picture to cut out interferences
            cv::Mat image_HSV;
            cv::cvtColor(image, image_HSV, cv::COLOR_BGR2HSV); // Converting image to HSV
            cv::Mat output;
            cv::inRange(
                image_HSV, cv::Scalar(0, 128, 20), cv::Scalar(10, 255, 250), output);
            int min_x = INT_MAX; // Declare maximum value of type integer
            cv::Point current_Pixel;

            // Iterate through every pixel and check whether it is the color we look for
            for (int y = 0; y < output.rows; y++) {
                for (int x = 0; x < output.cols; x++) {
                    if (output.at<uchar>(y, x) == 255) {
                        if (min_x > x) {
                            min_x = x;
                            current_Pixel = cv::Point(x, y);
                        }
                    }
                }
            }
            if (min_x != INT_MAX) {
                current_Pixel = current_Pixel + offset;

                if (current_Pixel.x > 600) {
                // We check this so the projectile doesnt fly to long
                    pressOne(current_Pixel.x - 95, current_Pixel.y);
                } else {
                    pressOne(current_Pixel.x - 60, current_Pixel.y);
                }
            }
            // Get time and convert it to seconds
            t2 = Clock::now();
            time = (std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1000000000.0);
        }
        clickAt(705, 650); // click the continue button
    }
    return 0;
}
